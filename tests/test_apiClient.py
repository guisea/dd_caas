from unittest import TestCase
from nose.tools import raises

from dd_caas import ApiClient
import os


class TestApiClient(TestCase):

    def setUp(self):
        self.api_user = os.environ['api_user']
        self.api_password = os.environ['api_password']

    @raises(ValueError)
    def test_exception_if_params_missing(self):
        self.client = ApiClient(self.api_user)

    def test_instantiation(self):
        self.client = ApiClient(self.api_user,
                                self.api_password)
        self.assertIsInstance(self.client, ApiClient)
