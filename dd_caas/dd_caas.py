import requests
import urllib
try:
    from urllib.parse import urljoin
except ImportError:
    from urlparse import urljoin
import xml.etree.ElementTree as eT


class ApiClient(object):
    endpoint = 'https://api-au.dimensiondata.com/caas/2.1/'
    account_endpoint = 'https://api-au.dimensiondata.com'
    headers = {'Accept': 'application/json'}

    def __init__(self, api_user=None, api_pass=None):
        missing = list()
        if api_user is None:
            missing.append('api_user')
        if api_pass is None:
            missing.append('api_pass')
        if len(missing) > 0:
            message = ''.join(['Parameters missing: [',
                               ','.join(missing),
                               ']'
                               ])
            raise ValueError(message)
        self.api_user = api_user
        self.api_pass = api_pass
        self.org_id = self._get_orgid()

    def get_server(self, serverid):
        r = requests.get(urljoin(self.endpoint,
                                 '/'.join([self.org_id,
                                           'server',
                                           'server',
                                           serverid
                                           ]
                                          )
                                 ),
                         auth=(self.api_user,
                               self.api_pass),
                         headers=self.headers
                         )

        return r.json()

    def _get_orgid(self):
        apiuri = '/oec/0.9/myaccount'
        url = self.account_endpoint + apiuri

        r = requests.get(url,
                         auth=(self.api_user,
                               self.api_pass),
                         headers=self.headers
                         )

        if r.status_code == 200:
            root = eT.fromstring(r.text)
            ns = {'directory': 'http://oec.api.opsource.net/schemas/directory'}
            return root.find('directory:orgId', ns).text
        else:
            root = eT.fromstring(r.text)
            ns = {'error': 'http://oec.api.opsource.net/schemas/organization'}
            raise Exception(root.find('error:resultDetail', ns).text)
        
    def list_servers(self, filter=None, paging_ordering=None):
        # type: (object, object) -> object

        """ Return a list of Dimension Data servers
        :rtype: dict
        :param filter: Filter options to urlencode.  e.g {'name':'Some Server Name'}

        Filter Optional Parameters:
        [&id=]
        [&datacenterId=]
        [&networkDomainId=]
        [&networkId=]
        [&vlanId=]
        [&sourceImageId=]
        [&deployed=]
        [&name=]
        [&createTime=]
        [&state=]
        [&started=]
        [&operatingSystemId=]
        [&ipv6=]
        [&privateIpv4=]

        :param paging_ordering:

        Paging/Ordering Optional Parameters:
        [&pageSize=]
        [&pageNumber=]
        [&orderBy=]
        :return: JSON
        """
        f = ''
        p = list()

        if filter is not None:
            f += urllib.urlencode(filter)
        if paging_ordering is not None:
            if filter is not None:
                p.append('&')
            p.append(urllib.urlencode(paging_ordering))

        r = requests.get(urljoin(self.endpoint,
                                 '/'.join([self.org_id,
                                           'server',
                                           'server',
                                           '?',
                                           f,
                                           ''.join(p)])),
                         auth=(self.api_user,
                               self.api_pass),
                         headers=self.headers
                         )

        return r.json()

