from distutils.core import setup

setup(
    name='dd_caas',
    version='0.1',
    packages=[''],
    package_dir={'': 'dd_caas'},
    url='',
    license='MIT',
    author='Aaron Guise',
    author_email='aaron@guise.net.nz',
    description='REST API Client for Dimension Data MCP',
    install_requires=['requests']
)
